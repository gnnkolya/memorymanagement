using System.Threading.Tasks;
using MassTransit;

namespace MessagesClient
{
    internal class MessageClient : IMessageClient
    {
        private readonly RabbitMqConfiguration _rabbitMqConfiguration;
        private readonly  IPublishEndpoint _sendEndpoint;

        public MessageClient(RabbitMqConfiguration rabbitMqConfiguration)
        {
            _rabbitMqConfiguration = rabbitMqConfiguration;

            _sendEndpoint = Bus.Factory.CreateUsingRabbitMq(c => c.Host(_rabbitMqConfiguration.Host, hostConfiguration =>
            {
                hostConfiguration.Username(_rabbitMqConfiguration.Username);
                hostConfiguration.Password(_rabbitMqConfiguration.Password);
            }));
        }
        

        Task IMessageClient.SendMessageAsync(IMessage message)
        {
            return _sendEndpoint.Publish(message);
        }
    }
}